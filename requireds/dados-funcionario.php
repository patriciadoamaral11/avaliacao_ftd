<?php	
	define('URL', "/avaliacao_ftd");
	define('RAIZ', $_SERVER['DOCUMENT_ROOT'] . "/avaliacao_ftd/");

    //Classes
    include(RAIZ."/classes/bd.php"); $Bd = new Bd();
    include(RAIZ."/classes/funcionario.php"); $Funcionario = new Funcionario();
    include(RAIZ."/classes/departamento.php"); $Departamento = new Departamento();

    $query = "SELECT E.*, S.salary, D.dept_no, D.dept_name, T.title
				FROM employees E
				INNER JOIN salaries S ON S.emp_no = E.emp_no
				INNER JOIN dept_emp DE ON DE.emp_no = E.emp_no
				INNER JOIN departments D ON D.dept_no = DE.dept_no
				INNER JOIN titles T ON T.emp_no = E.emp_no
				WHERE E.emp_no='{$_POST['emp_no']}'
              ";

    $stmt = $Bd->conn->prepare($query);
    $stmt->execute();
    $stmt->setFetchMode(PDO::FETCH_OBJ);

    $v = $stmt->fetch();
   /* 
    if(isset($_POST['alterar']) && isset($_POST['id_item'])){
		if($Funcionario->update($_POST)){
			//echo "<script>location.href = location.href;</script>";
	  	}
	}*/
    ?>

    <form method="POST" action="">
		        	
		        	<div class="row form-group">

		        		<div class="col">
						    <label for="first_name" class="col col-form-label">Nome:</label>
							<div class="col">
						      <input type="text" name="first_name" id="first_name" class="form-control form-control-sm" value="<?=$v->first_name?>" required="">
						    </div>

						    <label for="last_name" class="col col-form-label">Sobrenome:</label>					
							<div class="col">
						      <input type="text" name="last_name" id="last_name" class="form-control form-control-sm" required="" value="<?=$v->last_name?>">
						    </div>

						    <label for="gender" class="col col-form-label">Gênero:</label>
							<div class="col">
						      <select name='gender' class="form-control form-control-sm">
						      	<option value=''>Selecione</option>
						      	<option value='M' <?=($v->gender='M') ? 'selected':''?>>Masculino</option>
						      	<option value='F' <?=($v->gender='F') ? 'selected':''?>>Feminino</option>
						      </select>
						    </div>

						    <label for="birth_date" class="col col-form-label">Data Nasc.:</label>					
							<div class="col">
						      <input type="date" name="birth_date" id="birth_date" class="form-control form-control-sm" required="" value="<?=$v->birth_date?>">
						    </div>
						</div>

						<div class="col">
						    <label for="dept_no" class="col col-form-label">Depto:</label>
						    <div class="col">
							    <select name='dept_no' class='form-control form-control-sm'>";
					    			<option name=''>Selecione</option>
					    			<?php
					    			foreach($Departamento->get() as $d){
					    			echo "<option value='{$d->dept_no}' ".(($v->dept_no == $d->dept_no) ? 'selected':'')." >{$d->dept_name}</option>";
					    			}
					    			?>
				    			</select>
			    			</div>	

						    <label for="title" class="col col-form-label">Cargo:</label>
							<div class="col">
						      <input type="text" name="title" id="title" class="form-control form-control-sm" required="" value="<?=$v->title?>">
						    </div>
						    
						    <label for="salary" class="col col-form-label">Salário:</label>
							<div class="col">
						      <input type="text" name="salary" id="salary" class="form-control form-control-sm" required="" placeholder="1.200,00" value="<?=$v->salary?>">
						    </div>				    	    

						    <label for="hire_date" class="col col-form-label">Data Contratação:</label>					
							<div class="col">
						      <input type="date" name="hire_date" id="hire_date" class="form-control form-control-sm" required="" value="<?=$v->hire_date?>">
						    </div>
						</div>

					</div>

					<div class="row form-group">
						<div class="col">
						<input type="submit" name="alterar" value="Salvar" class="btn btn-info btn-sm float-right mr-3"/>
						</div>
					</div>
					
					<input type="hidden" name="id_item" value="<?=$v->emp_no?>">
	        	</form>