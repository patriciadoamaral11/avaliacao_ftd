<?php

class Acesso extends Bd
{

    public function __construct()
    {
        parent::__construct();

        $_SG['abreSessao'] = true;         // Inicia a sessão com um session_start()?
        $_SG['caseSensitive'] = false;     // Usar case-sensitive? Onde 'thiago' é diferente de 'THIAGO'
        $_SG['validaSempre'] = true;       // Deseja validar o usuário e a senha a cada carregamento de página?

        /* Verificar SESSION - Depende das configurações do PHP
        _DISABLED = 0 //SESSIONS desabilitadas
        _NONE = 1 //Habilitado, mas não existe
        _ACTIVE = 2 //Habilitado e existe
        */

        if(session_status() == 1){
            session_start();
        }
    }

    function validaUsuario($usuario, $senha)
    {
        global $_SG;

        $cS = ($_SG['caseSensitive']) ? 'BINARY' : '';

        $query = "SELECT E.*, U.profile 
                  FROM user U 
                  INNER JOIN employees E
                    ON E.emp_no = U.emp_no
                  WHERE U.user_name = '{$usuario}' AND U.password = '{$senha}' AND U.status='1'
                  ";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_OBJ);

        if ($stmt->rowCount() == 0) {

            return false;

        } else {

            $r = $stmt->fetch();

            //session_regenerate_id();

            $_SESSION['USUARIO']['acessoPerfil']   = $r->profile;
            $_SESSION['USUARIO']['acessoId']       = $r->emp_no;
            $_SESSION['USUARIO']['acessoNome']     = $r->first_name." ".$r->last_name;
            $_SESSION['USUARIO']['acessoUsuario']  = $usuario;
            $_SESSION['USUARIO']['acessoSenha']    = $senha;
        
            return true;
        }
    }

    function protegePagina()
    {
        global $_SG;

        if (!isset($_SESSION['USUARIO']['acessoUsuario']) OR !isset($_SESSION['USUARIO']['acessoSenha'])) {

            $this->expulsaVisitante("Erro1");

        } else {

            if ($_SG['validaSempre'] == true) {

                if (!$this->validaUsuario($_SESSION['USUARIO']['acessoUsuario'], $_SESSION['USUARIO']['acessoSenha'])) {
                    $this->expulsaVisitante("Erro2");
                }

            }
        }
    }    

    function expulsaVisitante($msg)
    {
        unset($_SESSION["USUARIO"]);

        header("Location: ".URL."/login/{$msg}");
    }

}
