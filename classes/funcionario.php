<?php

class Funcionario extends Bd
{

    public function __construct()
    {
        parent::__construct('employees');
    }

    function getFuncionarios(){
		$query = "	SELECT E.*, S.salary, D.dept_name, T.title
					FROM employees E
					INNER JOIN salaries S ON S.emp_no = E.emp_no
					INNER JOIN dept_emp DE ON DE.emp_no = E.emp_no
					INNER JOIN departments D ON D.dept_no = DE.dept_no
					INNER JOIN titles T ON T.emp_no = E.emp_no
				";
        
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_OBJ);

        if ($stmt->rowCount() == 0) {
            return false;
        } else {
            return $stmt->fetchAll();
        }
    }

    function procurar($first_name, $dept_no, $birth_date){
		$query = "	SELECT E.*, S.salary, D.dept_name, T.title
					FROM employees E
					INNER JOIN salaries S ON S.emp_no = E.emp_no
					INNER JOIN dept_emp DE ON DE.emp_no = E.emp_no
					INNER JOIN departments D ON D.dept_no = DE.dept_no
					INNER JOIN titles T ON T.emp_no = E.emp_no
					WHERE E.first_name='{$first_name}' AND DE.dept_no='{$dept_no}' AND E.birth_date='{$birth_date}'
				";
        
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_OBJ);

        if ($stmt->rowCount() == 0) {
            return false;
        } else {
            return $stmt->fetchAll();
        }
    }

    function cadastrar($post){ 		
    	
    	$arr1=array(
    		"birth_date"=>$post['birth_date'], 
    		"first_name"=>$post['first_name'], 
    		"last_name"=>$post['last_name'], 
    		"gender"=>$post['gender'], 
    		"hire_date"=>$post['hire_date'], 
    		"tabela" => "employees");
    	
    	if($this->insert($arr1)){

			$id_func = $this->getCompleto("emp_no", "employees", null, "emp_no DESC", "1")[0]->emp_no; 

	    	$arr2=array("emp_no"=>$id_func, "title"=>$post['title'], "from_date"=>date("Y-m-d"), "tabela" => "titles");

	    	$arr3=array("emp_no"=>$id_func, "salary"=>$post['salary'], "from_date"=>date("Y-m-d"), "tabela" => "salaries");

	    	$arr4=array("emp_no"=>$id_func, "dept_no"=>$post['dept_no'], "tabela" => "dept_emp");

	    	if($this->insert($arr2) && $this->insert($arr3) && $this->insert($arr4)){
	    		return true;
	    	}else{
				return false;
			}
		}else{
			return false;
		}
    	
	}

	function alterar($post){		
    	
    		$arr1=array(
    		"id_item"=>$post['id_item'],
    		"birth_date"=>$post['birth_date'], 
    		"first_name"=>$post['first_name'], 
    		"last_name"=>$post['last_name'], 
    		"gender"=>$post['gender'], 
    		"hire_date"=>$post['hire_date'], 
    		"tabela" => "employees"
    		);    	
    	
			$arr2=array("id_item"=>$post['id_item'], "title"=>$post['title'], "from_date"=>date("Y-m-d"), "tabela" => "titles");

	    	$arr3=array("id_item"=>$post['id_item'], "salary"=>$post['salary'], "from_date"=>date("Y-m-d"), "tabela" => "salaries");

	    	$arr4=array("id_item"=>$post['id_item'], "dept_no"=>$post['dept_no'], "tabela" => "dept_emp");

	    	if($this->update($arr1) && $this->update($arr2) && $this->update($arr3) && $this->update($arr4)){
	    		return true;
	    	}else{
				return false;
			}		
    	
	}

	function deletar($emp_no){		
    	
    	if($this->delete($emp_no, "employees") 
	    		&& $this->delete($emp_no, "titles") 
	    		&& $this->delete($emp_no, "salaries") 
	    		&& $this->delete($emp_no, "dept_emp")){
	 		return true;
		}else{
			return false;
		}		
    	
	}

	function funcionarioSemUsuario(){
		$query = "SELECT E.* FROM employees E
		NATURAL LEFT join user U
		WHERE U.emp_no IS NULL
		ORDER BY E.first_name
		";

		$stmt = $this->conn->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_OBJ);

        if ($stmt->rowCount() == 0) {
            return false;
        } else {
            return $stmt->fetchAll();
        }
	}


}