<?php

class Bd
{

    private $tabela;
    public $conn;

    public function __construct($tabela = null)
    {
        /**A tabela pode ser definida na classe para ser utilizada aqui nas funções genéricas**/
        if (!is_null($tabela)) {
            $this->tabela = $tabela;
        }

        /**CONEXÃO**/
        try {

            $this->conn = new PDO("mysql:host=bdavaliacao.cox8khh0ixs0.sa-east-1.rds.amazonaws.com;dbname=bdavaliacao;charset=utf8", 'bdavaliacao', 'bdavaliacao');

            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);// set the PDO error mode to exception
            //echo "Conectado com sucesso";
            
        } catch (PDOException $e) {
            echo "<p>Falha na conexão: " . $e->getMessage() . "</p>";
            print_r($this->conn);
        }
    }

    /**FUNÇÕES GENÉRICAS**/

    function get($id = null)
    {
        if($this->tabela == "departments"){
            $chave = "dept_no";
        }
        if($this->tabela == "employees"){
            $chave = "emp_no";
        }

        $query = "SELECT * FROM {$this->tabela}";
        if (!is_null($id)) {
            $query .= " WHERE {$chave} = {$id}";
        }

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_OBJ);

        if ($stmt->rowCount() == 0) {
            return false;
        } else {
            return $stmt->fetchAll();
        }
    }

    function getCompleto($campos = null, $tabela = null, $where = null, $order = null, $limit = null)
    {
        $query = "SELECT ";

        if (!is_null($campos)) {
            $query .= $campos;
        } else {
            $query .= "*";
        }

        if (!is_null($tabela)) {
            $query .= " FROM " . $tabela;
        } else {
            $query .= " FROM " . $this->tabela;
        }

        if (!is_null($where)) {
            $query .= " WHERE " . $where;
        }
        if (!is_null($order)) {
            $query .= " ORDER BY " . $order;
        }
        if (!is_null($limit)) {
            $query .= " LIMIT " . $limit;
        }

        //echo $query;
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_OBJ);

        if ($stmt->rowCount() == 0) {
            return false;
        } else {
            return $stmt->fetchAll();
        }
    }

    function insert($post)
    {
        //Definindo tabela
        if (isset($post['tabela'])) {
            $tabela = $post['tabela'];
        } else {
            $tabela = $this->tabela;
        }

        unset($post['tabela']);
        unset($post['inserir']);

        try {
            $query = "INSERT INTO {$tabela} (";

            foreach ($post as $v => $value) {
                $keys = array_keys($post);
                if (end($keys) != $v) {//Enquanto não for o final do array, coloca vírgula
                    $query .= $v . ", ";

                } else {
                    $query .= $v . ") VALUES ( ";

                    //Roda de novo para pegar o valores do post
                    foreach ($post as $v => $value) { //if(empty($value)){$value=NULL;}
                        $keys = array_keys($post);
                        if (end($keys) != $v) {//Enquanto não for o final do array, coloca vírgula
                            if (empty($value)) {
                                $query .= "NULL, ";
                            } else {
                                $query .= "'" . $value . "', ";
                            }
                        } else {
                            $query .= "'" . $value . "')";
                        }
                    }
                }
            }
            //print $query;
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            return true;

        } catch (PDOException $e) {

            if ($e->getCode() == "23000") {
                echo "<div class='alert alert-danger' role='alert'>Ooops! Já temos esse registro! ";
                echo $e->getMessage();
                echo "</div>";
            } else {
                echo "<div class='alert alert-danger' role='alert'>Erro desconhecido: ";
                echo $e->getMessage();
                echo "</div>";
            }

            return false;
        }
    }

    function update($post)
    {
        //Definindo tabela
        if (isset($post['tabela'])) {
            $tabela = $post['tabela'];
        } else {
            $tabela = $this->tabela;
        }

        $id = $post['id_item'];

        if($tabela == "departments"){
            $chave = "dept_no";
        }
        if($tabela == "employees" || $tabela == "titles" || $tabela == "salary" || $tabela == "dept_emp"){
            $chave = "emp_no";
        }

        //Tira para não ir para o UPDATE
        unset($post['id_item']);
        unset($post['alterar']);
        unset($post['tabela']);

        try {
            //Faz Update
            $query = "UPDATE " . $tabela . " SET ";

            foreach ($post as $v => $value) {
                $keys = array_keys($post);
                if (end($keys) != $v) {//Enquanto não for o final do array, coloca vírgula
                    $query .= $v . "= '" . $value . "', ";
                } else {
                    $query .= $v . "= '" . $value . "' ";
                    $query .= " WHERE {$chave}='" . $id . "' ";
                }
            }

            //echo $query;die();
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            return true;
            //return "<div class='alert alert-success' role='alert'>Atualizado com sucesso</div>";

        } catch (PDOException $e) {
            return "<div class='alert alert-danger' role='alert'>Erro: " . $e->getMessage() . "</div>";
        }

    }

    function delete($tabela, $id_item)
    {
        //DEFINIR retorno
        $retorno = $_SERVER['HTTP_REFERER'];

        if($tabela == "departments"){
            $chave = "dept_no";
        }
        if($tabela == "employees" || $tabela == "titles" || $tabela == "salary" || $tabela == "dept_emp"){
            $chave = "emp_no";
        }

        unset ($_POST['deletar']);

        $delete = "DELETE FROM {$tabela} WHERE {$chave} = {$id_item} ";
print_r($delete);die();
        if ($this->conn->query($delete) === false) {
            return "<div class='alert alert-danger' role='alert'>Erro: Não foi possível excluir.</div>";
        } else {
            //return "<script>location.href= '" . $retorno . "';</script>";
        }
        
    }    

}