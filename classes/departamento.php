<?php

class Departamento extends Bd
{

    public function __construct()
    {
        parent::__construct('departments');
    }

    function somaFuncionario($dept){
		$query = "	SELECT SUM(emp_no) AS soma 
					FROM dept_emp 
					WHERE dept_no = '{$dept}'";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_OBJ);

        $resp = $stmt->fetch()->soma;

        if (empty($resp)) {
            return "0";
        } else {
            return $resp;
        }
    }

}