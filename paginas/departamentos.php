<?php
//Controle de Acesso
include(RAIZ."classes/acesso.php"); $Acesso = new Acesso(); $Acesso->protegePagina();

include(RAIZ."/includes/head.php");
include(RAIZ."/includes/topo.php");

//Classes
include(RAIZ."/classes/departamento.php"); $Departamento = new Departamento();
?>

<div class="container">

    <p class="display-4">Departamentos</p>
    
    <div class="row col-md-4">

        <div class="card bg-light mb-3">
		  <div class="card-header">Cadastrar</div>
		  <div class="card-body">

		    <form method="POST" action="">
	        	<div class="row">
				    <label for="dept_name" class="col col-form-label">Nome:</label>
					<div class="col-sm-6">
				      <input type="text" name="dept_name" id="dept_name" class="form-control form-control-sm" required="">
				    </div>
				    
				    <div class="col">
				    	<input type="submit" name="inserir" value="Salvar" class="btn btn-info btn-sm"/>
				    </div>
				</div>
        	</form>

		  </div>
		</div>

		<?php
			if(isset($_POST['inserir'])){
	        	if($Departamento->insert($_POST, "")){
	        		echo "<div class='alert alert-success' role='alert'>Cadastro realizado com sucesso!</div>";
	        	}else{
					echo "<div class='alert alert-danger' role='alert'>Erro ao cadastrar.</div>";
	        	}
	        }

	        if(isset($_POST['alterar']) && isset($_POST['id_item'])){
	        	if($Departamento->update($_POST)){
                    echo "<script>location.href = location.href;</script>";
                }
            }

	        if(isset($_POST['deletar'])){
            	echo $Departamento->delete($_POST['tabela'], $_POST['id_item']);
        	}
		?>
	</div>

	<div class="row col">
		<?php 
	        $consulta = $Departamento->get();

	       	if($consulta == false){

	       		echo "Não existem departamentos cadastrados.";

	      	}else{

	      		echo "<table class='table table-hover'>";
	      		echo "<thead class='bg-light'>";
	      		echo "<tr>";
	      		echo "<th>Nome</th>";
	      		echo "<th>Qtde funcionários</th>";
	      		echo "</tr>";
	      		echo "</thead>";
	      		
	    		foreach ($consulta as $v) {
	    			echo "<tr>";

	    			echo "<td>";

	    				echo "<form action='' method='POST'>";

		    			echo "<div class='row'>";

		    			echo "<div class='col'>";
		    			
		    			echo "<input type='text' name='dept_name' class='form-control form-control-sm' value='{$v->dept_name}'>";

		    			echo "</div>";

		    			echo "<div class='col'>";
						
						echo "<button type='submit' name='alterar' class='btn btn-dark' title='Alterar'><i class='fa fa-pencil' aria-hidden='true'></i></button>";
		    			?>
		    				<button type="button" onclick="modalDeletar('departments', <?=$v->dept_no?>)" class="btn btn-danger" data-toggle="modal" data-target="#modal-confirma" title="Deletar">
							<i class="fa fa-trash-o" aria-hidden="true"></i>
		    				</button>
						<?php					
						echo "<input type='hidden' name='id_item' value='{$v->dept_no}'>";

						echo "</form>";

						echo "</div>";

						echo "</div>";

	    			echo "</td>";   			
	    			
	    			echo "<td>".$Departamento->somaFuncionario($v->dept_no)."</td>";  

	    			echo "</tr>";
	    		}

	    		echo "</table>";
	    	}
        ?>
    </div>

</div>

<?php include(RAIZ."/includes/footer.php");