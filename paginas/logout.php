<?php
//session_name("administrativo");
session_start();

unset($_SESSION['USUARIO']["acessoLogin"]); // desselecciona a variável
unset($_SESSION['USUARIO']);
unset($_SESSION);
session_destroy(); // detroy it

if(empty($_SESSION['USUARIO']["acessoLogin"])){
	header("location: login"); // vai para a pagina login.html
}
?>