<?php
//Controle de Acesso
include(RAIZ."classes/acesso.php"); $Acesso = new Acesso(); $Acesso->protegePagina();

include(RAIZ."/includes/head.php");
include(RAIZ."/includes/topo.php");

//Classes
include(RAIZ."/classes/funcionario.php"); $Funcionario = new Funcionario();
include(RAIZ."/classes/departamento.php"); $Departamento = new Departamento();
?>

<div class="container">

	<p class="display-4">Intranet</p>

	<div class="row">
		<div class="card bg-light mb-3">
			<h5 class="card-header">Faça sua pesquisa</h5>
			<div class="card-body">
				<form method="POST" action="">
					<div class="row">
						<div class="col">
							<input type="text" name="first_name" class="form-control form-control-sm" required="" placeholder="Nome">
						</div>
						<div class="col">
							<select name='dept_no' class='form-control form-control-sm'>
								<option value="">Departamento...</option>
		    					<?php
		    					foreach($Departamento->get() as $d){
		    					echo "<option value='{$d->dept_no}'".($v->dept_no == $d->dept_no ? 'selected':'').">{$d->dept_name}</option>";
		    					}
		    					?>
		    				</select>
						</div>
						<div class="col">
							<input type="date" name="birth_date" class="form-control form-control-sm" required="">
						</div>
						<div class="col">
							<button type="submit" name="procurar" class="btn btn-info btn-sm"/>Procurar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="alert alert-warning" role="alert">Para teste tente: Pedro, Recursos Humanos, 03/03/1990</div>

	<?php
	if(isset($_POST['procurar'])){   
		
		$consulta = $Funcionario->procurar($_POST['first_name'], $_POST['dept_no'], $_POST['birth_date']);
		
		if($consulta == false){
			echo "<div class='alert alert-danger' role='alert'>";
			echo "Nada foi encontrado com os dados informados.<br>"; 
			echo "Nome: {$_POST['first_name']}, Departamento: ".$Departamento->get($_POST['dept_no'])[0]->dept_name.", Data Nasc.: {$_POST['birth_date']}";
			echo "</div>";
			unset($_POST['procurar']);

		}else{
			echo "<table class='table table-hover'>";
			echo "<tr>";
      		echo "<th>Nome Completo</th>"; 
      		echo "<th>Data Nascimento</th>";
      		echo "<th>Gênero</th>";     		
      		echo "<th>Departamento</th>";
      		echo "<th>Salário</th>";
      		echo "<th>Cargo</th>";
      		echo "<th>Contratação</th>";
      		echo "</tr>";			
			foreach ($consulta as $v) {
				echo "<tr>";
				echo "<td>{$v->first_name} {$v->last_name}</td>"; 
				echo "<td>{$v->birth_date}</td>"; 
				echo "<td>{$v->gender}</td>";   		
	      		echo "<td>{$v->dept_name}</td>";
	      		echo "<td>{$v->salary}</td>";
	      		echo "<td>{$v->title}</td>";
	      		echo "<td>{$v->hire_date}</td>";
	      		echo "</tr>";			
			}
			echo "</table>";
		}

	}
	?>	

</div>

<?php include(RAIZ."/includes/footer.php");
