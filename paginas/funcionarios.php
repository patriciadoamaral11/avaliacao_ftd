<?php
//Controle de Acesso
include(RAIZ."classes/acesso.php"); $Acesso = new Acesso(); $Acesso->protegePagina();

include(RAIZ."/includes/head.php");
include(RAIZ."/includes/topo.php");

//Classes
include(RAIZ."/classes/funcionario.php"); $Funcionario = new Funcionario();
include(RAIZ."/classes/departamento.php"); $Departamento = new Departamento();
?>

<div class="container">

    <p class="display-4">Funcionários</p>
    
    <div class="row">
    	<div class="col-md-6">

	        <div class="card bg-light mb-3">
			  <div class="card-header">Cadastrar</div>
			  <div class="card-body">

			    <form method="POST" action="">
		        	
		        	<div class="row form-group">

		        		<div class="col">
						    <label for="first_name" class="col col-form-label">Nome:</label>
							<div class="col">
						      <input type="text" name="first_name" id="first_name" class="form-control form-control-sm" required="">
						    </div>

						    <label for="last_name" class="col col-form-label">Sobrenome:</label>					
							<div class="col">
						      <input type="text" name="last_name" id="last_name" class="form-control form-control-sm" required="">
						    </div>

						    <label for="gender" class="col col-form-label">Gênero:</label>
							<div class="col">
						      <select name='gender' class="form-control form-control-sm">
						      	<option value=''>Selecione</option>
						      	<option value='M'>Masculino</option>
						      	<option value='F'>Feminino</option>
						      </select>
						    </div>

						    <label for="birth_date" class="col col-form-label">Data Nasc.:</label>					
							<div class="col">
						      <input type="date" name="birth_date" id="birth_date" class="form-control form-control-sm" required="">
						    </div>
						</div>

						<div class="col">
						    <label for="dept_no" class="col col-form-label">Depto:</label>
						    <div class="col">
							    <select name='dept_no' class='form-control form-control-sm'>";
					    			<option name=''>Selecione</option>
					    			<?php
					    			foreach($Departamento->get() as $d){
					    			echo "<option value='{$d->dept_no}'>{$d->dept_name}</option>";
					    			}
					    			?>
				    			</select>
			    			</div>	

						    <label for="title" class="col col-form-label">Cargo:</label>
							<div class="col">
						      <input type="text" name="title" id="title" class="form-control form-control-sm" required="">
						    </div>
						    
						    <label for="salary" class="col col-form-label">Salário:</label>
							<div class="col">
						      <input type="text" name="salary" id="salary" class="form-control form-control-sm" required="" placeholder="1.200,00">
						    </div>				    	    

						    <label for="hire_date" class="col col-form-label">Data Contratação:</label>					
							<div class="col">
						      <input type="date" name="hire_date" id="hire_date" class="form-control form-control-sm" required="">
						    </div>
						</div>

					</div>

					<div class="row form-group">
						<div class="col">
						<input type="submit" name="inserir" value="Salvar" class="btn btn-info btn-sm float-right mr-3"/>
						</div>
					</div>

	        	</form>

			  </div>
			</div>

			<?php
				if(isset($_POST['inserir'])){      	

					if($Funcionario->cadastrar($_POST)){
		        		echo "<div class='alert alert-success' role='alert'>Cadastro realizado com sucesso!</div>";
		        	}else{
						echo "<div class='alert alert-danger' role='alert'>Erro ao cadastrar.</div>";
		        	}
		        }

		        if(isset($_POST['alterar']) && isset($_POST['id_item'])){
		        	if($Funcionario->alterar($_POST)){
		        		 echo "<script>location.href = location.href;</script>";
		        	}else{
						echo "<div class='alert alert-danger' role='alert'>Erro ao cadastrar.</div>";
		        	}
	            }

		        if(isset($_POST['deletar'])){
	            	if($Funcionario->deletar($_POST['id_item'])){
		        		 echo "<script>location.href = location.href;</script>";
		        	}else{
						echo "<div class='alert alert-danger' role='alert'>Erro ao cadastrar.</div>";
		        	}
	        	}
			?>

		</div>
	</div>

	<div class="row">
		<div class="col">
		<?php 
	        $consulta = $Funcionario->getFuncionarios();

	       	if($consulta == false){

	       		echo "Não existem funcionários cadastrados.";

	      	}else{

	      		echo "<table class='table table-striped'>";
	      		echo "<thead class='bg-light'>";
	      		echo "<tr>";
	      		echo "<th>Nome Completo</th>"; 
	      		echo "<th>Data Nascimento</th>";
	      		echo "<th>Gênero</th>";     		
	      		echo "<th>Departamento</th>";
	      		echo "<th>Salário</th>";
	      		echo "<th>Cargo</th>";
	      		echo "<th>Contratação</th>";
	      		echo "<th>Ação</th>";
	      		echo "</tr>";    
	      		echo "</thead>";  		
	     		
	    		foreach ($consulta as $v) {
	    			
	    			echo "<tr>";

	    			echo "<td>{$v->first_name}</td>";

		    		echo "<td>{$v->birth_date}</td>";

		    		echo "<td>{$v->gender}</td>";	    		
	
		    		echo "<td>{$v->dept_name}</td>";

		    		echo "<td>{$v->salary}</td>";

		    		echo "<td>{$v->title}</td>";

		    		echo "<td>{$v->hire_date}</td>";

		    		echo "<td>"; 					
					echo "<a id='{$v->emp_no}' href='?emp_no={$v->emp_no}' class='alterar btn btn-dark' title='Alterar' data-toggle='modal' data-target='#modal-alterar-func'>";
					echo "<i class='fa fa-pencil' aria-hidden='true'></i>";
					echo "</a>";

		    		?>
		    			<button type="button" onclick="modalDeletar('employees', <?=$v->emp_no?>)" class="btn btn-danger" data-toggle="modal" data-target="#modal-confirma" title="Deletar">
						<i class="fa fa-trash-o" aria-hidden="true"></i>
		    			</button>
					<?php					
					echo "</td>";					
		    		
		    		echo "</tr>";
	    		}

    			echo "</table>";    		
    		}
        ?>
        </div>
    </div>

</div>

<?php include(RAIZ."/includes/footer.php");