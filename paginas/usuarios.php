<?php
//Controle de Acesso
include(RAIZ."classes/acesso.php"); $Acesso = new Acesso(); $Acesso->protegePagina();

include(RAIZ."/includes/head.php");
include(RAIZ."/includes/topo.php");

//Classes
include(RAIZ."/classes/usuario.php"); $Usuario = new Usuario();
include(RAIZ."/classes/funcionario.php"); $Funcionario = new Funcionario();
?>

<div class="container">

    <p class="display-4">Usuários</p>
   
    <div class="row col-md-4">

    	<div class="card bg-light mb-3">
    		<div class="card-header">Cadastrar</div>
    		<div class="card-body">

    			<form method="POST" action="">
    				
    				<div class="row">
    					
    					<label for="dept_name" class="col-sm-6 col-form-label">Funcionário:</label>
    					<div class="col-sm-6">
    						<select name="emp_no" class="form-control">
    							<?php foreach ($Funcionario->funcionarioSemUsuario() as $v) {
    							echo "<option value='{$v->emp_no}'>".$v->first_name." ".$v->last_name."</option>";
    							}
    							?>
    						</select>
    					</div>

    					<label for="dept_name" class="col-sm-6 col-form-label">Perfil:</label>
    					<div class="col-sm-6">
    						<select name="profile" class="form-control">
    							<option value=''>Selecione...</option>
    							<option value='Manager'>Administrador</option>
    							<option value='User'>Usuário</option>
    						</select>
    					</div>

    					<label for="password" class="col-sm-6 col-form-label">Senha:</label>
    					<div class="col-sm-6">
    						<input type="password" name="password" class="form-control"/>
    					</div>

    				</div>

    				<div class="row form-group">
    					<div class="col">
    						<input type="submit" name="inserir" value="Salvar" class="btn btn-info btn-sm float-right mt-3"/>
    					</div>
    				</div>

    			</form>

    		</div>
    	</div>

		<?php
			if(isset($_POST['inserir'])){
	        	if($Usuario->cadastrar($_POST)){
		        		echo "<div class='alert alert-success' role='alert'>Cadastro realizado com sucesso!</div>";
		        	}else{
						echo "<div class='alert alert-danger' role='alert'>Erro ao cadastrar.</div>";
		        	}
	        }

	        if(isset($_POST['alterar']) && isset($_POST['id_item'])){
	        	if($Usuario->update($_POST)){
                    echo "<script>location.href = location.href;</script>";
                }
            }

	        if(isset($_POST['deletar'])){
            	echo $Usuario->delete($_POST['tabela'], $_POST['id_item']);
        	}
		?>
	</div>


	<div class="row col">
		<?php 
	        $consulta = $Usuario->get();

	       	if($consulta == false){

	       		echo "Não existem usuários cadastrados.";

	      	}else{

	      		echo "<table class='table table-hover'>";
	      		echo "<thead class='bg-light'>";
	      		echo "<tr>";
	      		echo "<th>Nome de usuário</th>";
	      		echo "<th>Profile</th>";
	      		echo "<th>Status</th>";
	      		echo "</tr>";
	      		echo "</thead>";
	      		
	    		foreach ($consulta as $v) {
	    			echo "<tr>";

	    			echo "<td>{$v->user_name}</td>";   			
	    			
	    			echo "<td>{$v->profile}</td>";   	

	    			echo "<td>{$v->status}</td>";   	

	    			echo "</tr>";
	    		}

	    		echo "</table>";
	    	}
        ?>
    </div>

</div>

<?php include(RAIZ."/includes/footer.php");