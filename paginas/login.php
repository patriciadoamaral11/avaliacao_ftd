<?php
include(RAIZ."/includes/head.php");

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    include(RAIZ . "classes/acesso.php"); $Acesso = new Acesso();

    $usuario = $_POST['usuario'];
    $senha = md5($_POST['senha']);

    if (empty($usuario)) {
        $Acesso->expulsaVisitante("Erro1");

    } elseif (empty($senha)) {
        $Acesso->expulsaVisitante("Erro2");

    } else {

        if ($Acesso->validaUsuario($usuario, $senha) === true) {
            // O usuário e a senha digitados foram validados, manda pra página interna
            //var_dump($_SESSION);die();
            header("Location: ".URL."/index");

        } else {
            // O usuário e/ou a senha são inválidos, manda de volta pro form de login
            $Acesso->expulsaVisitante("Erro4");
        }

    }
}
?>

<body id="login-body" class="text-center">


    <form class="form-signin" action="" method="POST">
        
        <h1 class="h3 mb-4 font-weight-normal">Login Intranet</h1>
        
        <input type="text" name="usuario" class="form-control" placeholder="Usuário" required="" autofocus="" value="pedro.silva">
        <input type="password" name="senha" class="form-control" placeholder="Senha" required="" value="123">        
        <button class="btn btn-lg btn-info btn-block" type="submit">Entrar</button>
        
        <p class='alert alert-danger mt-3' role='alert'>
            <?php 
            switch($mensagem_login){
                case 'Erro1': echo "Informe o usuário corretamente."; break;
                case 'Erro2': echo "Informe uma senha corretamente."; break;
                case 'Erro3': echo "Não foi possível identificar o seu usuário, tente novamente."; break;
                case 'Erro4': echo "Usuário e/ou  senha inválidos. Tente novamente com os dados corretos."; break;
                default: echo "Preencha usuário e senha para logar.";
            }
            ?>
        </p>        

        <p class="mt-5 mb-3 text-muted">Candidata Patricia do Amaral © 2018</p>

    </form>      

</body>