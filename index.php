<?php ob_start();

define('URL', "/avaliacao_ftd");
define('RAIZ', $_SERVER['DOCUMENT_ROOT'] . "/avaliacao_ftd/");

include(RAIZ . "classes/bd.php"); //Para não declarar em todas as páginas, declarei aqui

$atual = (isset($_GET['pg'])) ? $_GET['pg'] : 'index';

$permissao = array('index', 'login', 'logout', 'erro404', 'departamentos', 'funcionarios', 'usuarios');

$pasta = "paginas";

    if (substr_count($atual, '/') > 0) {// Tem "/"
        $atual = explode('/', $atual);
        $pagina = (file_exists("{$pasta}/" . $atual[0] . '.php') && in_array($atual[0], $permissao)) ? $atual[0] : 'erro404';
        $mensagem_login = $atual[1];
    } else {
        $pagina = (file_exists("{$pasta}/" . $atual . '.php') && in_array($atual, $permissao)) ? $atual : 'erro404';
        $mensagem_login = $atual;
    }

    //$nivel_1 =  $atual[1];
    $nivel_2 = $atual[1];

require("{$pasta}/{$pagina}.php");
