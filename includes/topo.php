<?php $Bd = new Bd(); ?>

<div id="topo">

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">

        <a class="navbar-brand" href="<?=URL?>">Empresa Teste</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>


        <div id="navbar" class="collapse navbar-collapse" id="navbarNavDropdown">

            <ul class="nav navbar-nav mr-auto">
                <?php 
                $perfil = $_SESSION['USUARIO']['acessoPerfil'];
                if( $perfil == "Manager"){ ?>

                <li class="nav-item">
                    <a class="nav-link" href="departamentos">Departamentos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="funcionarios">Funcionários</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="usuarios">Usuários</a>
                </li>

                <?php }else{
                    echo "<li class='nav-item' style='color:red'>Você que tem o perfil de Usuário, poderá somente consultar.</li>";
                } ?>
            </ul>

            <ul class="nav navbar-nav">
                <li class="nav-item">
                    <div class="nav-link">
                        <?php echo "Bem vindo(a), ".$_SESSION['USUARIO']['acessoNome']; ?> | <a href="<?=URL?>/logout"> Sair </a>
                    </div>
                </li>
            </ul>

        </div>
    </nav>

</div>


