<!--Arquivo para definição geral de Modals-->

<script>
    function modalDeletar(tabela, id){

        $('#confirm').on('click', function(e)
        {
            var dados = {
                'deletar': '',
                'tabela': tabela,
                'id_item': id
            };

            $.ajax({
                type: 'POST',
                data: dados,

                success: function (data) {
                    $("#modal-confirma").modal("hide");

                    var str = location.href;
                    console.log("Sucesso :)");
                    location.href = str.replace('#', '');                    
                },
                error: function(data) {
                    $("#modal-confirma").modal("hide");
                    console.log("Erro: ");
                    console.log(data);
                }
            })
        })
    }
</script>

<script>
    $(document).ready(function() {
    $('#modal-alterar-func').on('show.bs.modal', function(e) { 
        
        var myId = e.relatedTarget.id;
        
        $.ajax({
            cache: false,
            type: 'POST',
            url: 'requireds/dados-funcionario.php',
            data: 'emp_no='+myId,
            success: function(data) 
            {
                $('#modal-alterar-func .modal-body').show().html(data);
            }
        });
    })
    });
</script>

<div class="modal fade" id="modal-alterar-func" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Alterar Funcionário</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-confirma" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Deletar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Tem certeza que deseja deletar?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Sair</button>
                <button type="button" id="confirm" class="btn btn-primary">Sim!</button>
            </div>
        </div>
    </div>
</div>